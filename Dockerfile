FROM python:3.9-slim-buster

ENV SM_URL=$SM_URL \
    SM_NAME=$SM_NAME \
    SM_TIMEOUT=$SM_TIMEOUT \
    TZ=$TZ

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY simple-monitoring.py /app/simple-monitoring.py

CMD ["python3", "/app/simple-monitoring.py"]