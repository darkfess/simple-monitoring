# simple-monitoring

Simple monitoring - lightweight, one command, one container, one web-resource to monitor.
<br><br>
See also: [Simple trigger](https://gitlab.com/darkfess/simple-trigger)
<br><br>

### Quickstart

    docker run -it -d --name simple-monitoring --restart=always \
      -v /home/darkfess/darkfess.conf:/app/config.conf \
      -e SM_URL=https://darkfess.ru \
      -e SM_NAME=DARKFESS \
      -e SM_TIMEOUT=60 \
      -e TZ=Europe/Moscow \
    darkfess/simple-monitoring

<br><hr><br>

### Enviroment Variables
Currently supported:
- SM_NAME
  - any name or id (example: DARKFESS)
- SM_URL
  - url to monitor (example: https://darkfess.ru)
- SM_TIMEOUT
  - timeout between requests, seconds (example: 60)
- TZ
  - (optional) timezone for log timestamps (example: Europe/Moscow)
<br><br>

### Volumes
Prepare your config before launch. Telegram bot required (and it **token**) and **chat_id** of targeted chat. You can create and use many configs.
- /YOUR-PATH/template.conf:/app/config.conf
  - your mounted config (includes **token** and **chat_id**)
  - use config template (just insert your values): [https://gitlab.com/darkfess/simple-monitoring/-/blob/main/template.conf](https://gitlab.com/darkfess/simple-monitoring/-/blob/main/template.conf)


<br><hr><br>

### Create your Telegram bot (optional)
Or how to receive **token**:
1. Write to BotFather: [https://telegram.me/BotFather](https://telegram.me/BotFather)
2. Create and configure your own bot
3. Receive bot **token**
<br><br>

### How to get my chat ID? (optional)
Here we get **chat_id** of your personal chat with a bot. Or add bot to a groupchat and receive its **chat_id**. 
<br><br>

Simple chat with bot:
1. Open in browser (don`t close this tab) and insert your **token** in this link: [https://api.telegram.org/bot<**token**>/getUpdates](https://api.telegram.org/bot<**token**>/getUpdates)
2. Write some message to this bot in Telegram
3. Refresh browser tab, get your **chat_id**
<br><br>

Adding bot to groupchat:
1. Open in browser (don`t close this tab) and insert your **token** in this link: [https://api.telegram.org/bot<**token**>/getUpdates](https://api.telegram.org/bot<**token**>/getUpdates)
2. Add bot to groupchat, write some messages there
3. Refresh browser tab, get your **chat_id**

<br><hr><br>

Docker Hub: [https://hub.docker.com/r/darkfess/simple-monitoring](https://hub.docker.com/r/darkfess/simple-monitoring)