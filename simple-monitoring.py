import os
import requests
import time
import datetime
import telegram_send

monitoring_url = os.environ['SM_URL']
service_name = os.environ['SM_NAME']
timeout = os.environ['SM_TIMEOUT']

### BASIC status marker
marker = True

def monitoring():
    global marker
    while True:

## request for status
        status = requests.get(monitoring_url).status_code

### IF [OK]
        if status == 200:

## print info
            print(datetime.datetime.now(), "| [ OK ]", service_name)

## if status marker was 'False', send [OK] tg-notification (but only 1)
            if marker == True:
                pass
            elif marker == False:
                telegram_send.send(conf="/app/config.conf", messages=["[OK] " + service_name])
## then change status marker to 'True'
            marker = True

### IF [FAIL]
        elif status >= 400:
## send [FAIL] tg-notification
            telegram_send.send(conf="/app/config.conf", messages=["[FAIL] " + service_name])
## print info
            print(datetime.datetime.now(), "| [ FAIL ]", service_name)
## change status marker to 'False'
            marker = False

## loop
        time.sleep(int(timeout))

monitoring()